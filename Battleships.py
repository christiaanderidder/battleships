import random

_board = []
_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
_tile_separator = " "

_tile_sea = "~"
_tile_hit = "x"
_tile_miss = "+"

_tile_ship = "o"
_tile_ship_end = "0"

_board_rows = 10
_board_cols = 10

"""
The available ships and their sizes
    1 x Aircraft carrier: 5 squares
    2 x Battleship: 4 squares
    3 x Submarine: 3 squares
    4 x Destroyer: 2 squares
"""
_available_ships = [5, 4, 4, 3, 3, 3, 2, 2, 2, 2]


"""
Generates an empty board
"""
def create_board(board_rows, board_cols):
    board = []
    for row in range(board_rows):
        board.append([_tile_sea] * board_cols)
    return board

"""
Prints the board in the following format:

   0 1 2 3 4

0  O O X O O
1  O O X O O
2  O O X O O
3  O O O O O
4  O X X O O

"""
def print_board(board, board_rows, board_cols):
    # Newline for readability
    print("") 

    for row_num in range(board_rows):
        row = board[row_num]
        if(row_num == 0):
            # Print A B C D E F G.. at the top row            
            print("   " + _tile_separator.join(_letters[:board_cols]))

            # Newline for readability
            print("") 

        # Print 3  O O X O X O O... for every row of the board
        print(str(row_num) + "  " + _tile_separator.join(row))

    # Newline for readability
    print("") 

def create_ships(available_ships, board, board_rows, board_cols):
    # Shuffle the list of available ships to make the placement more random
    random.shuffle(available_ships)

    for ship_size in available_ships:
        
        ship_x = 0
        ship_y = 0
        is_vertical = False
        is_valid_position = False
        algorithm_passes = 0

        # Keep looping until we have found a position that has not been taken yet.
        while(not is_valid_position):
            algorithm_passes += 1
            
            position = get_ship_position(ship_size, board_cols, board_rows)
            ship_x = position[0]
            ship_y = position[1]
            is_vertical = position[2]
            
            is_valid_position = is_ship_position_valid(ship_size, ship_x, ship_y, is_vertical, board)


        print("Algorithm passes needed: %s" % algorithm_passes)

        # We have found a vacant position, let's occupy it.
        if(is_vertical):
            for row_num in range(ship_y, ship_y + ship_size):
                tile = _tile_ship
                if(row_num == ship_y or row_num == ship_y + ship_size - 1):
                    tile = _tile_ship_end
                board[row_num][ship_x] = tile
        else:
            for col_num in range(ship_x, ship_x + ship_size):
                tile = _tile_ship
                if(col_num == ship_x or col_num == ship_x + ship_size - 1):
                    tile = _tile_ship_end
                board[ship_y][col_num] = tile

def is_ship_position_valid(ship_size, ship_x, ship_y, ship_is_vertical, board):

    if(ship_is_vertical):
        for row_num in range(ship_y, ship_y + ship_size):
            if(board[row_num][ship_x] == _tile_ship):
                return False
    else:
        for col_num in range(ship_x, ship_x + ship_size):
            if(board[ship_y][col_num] == _tile_ship):
                return False

    return True


def get_ship_position(ship_size, board_cols, board_rows):
    is_vertical = bool(random.getrandbits(1))
    ship_x = 0
    ship_y = 0

    if(is_vertical):
        ship_x = random.randint(0, board_cols - 1)
        ship_y = random.randint(0, board_rows - ship_size)
    else:
        ship_x = random.randint(0, board_cols - ship_size)
        ship_y = random.randint(0, board_rows - 1)    
    return [ship_x, ship_y, is_vertical]

_board = create_board(_board_rows, _board_cols)
create_ships(_available_ships, _board, _board_rows, _board_cols)
print_board(_board, _board_rows, _board_cols)